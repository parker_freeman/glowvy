module dimodo_backend

go 1.13

require (
	cloud.google.com/go v0.53.0
	cloud.google.com/go/firestore v1.1.1 // indirect
	cloud.google.com/go/storage v1.5.0
	github.com/EDDYCJY/fake-useragent v0.2.0
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/Timothylock/go-signin-with-apple v0.0.0-20200221193809-1a722422658e
	github.com/abadojack/whatlanggo v1.0.1
	github.com/algolia/algoliasearch-client-go/v3 v3.4.0
	github.com/antchfx/htmlquery v1.2.2 // indirect
	github.com/antchfx/xmlquery v1.2.3 // indirect
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/bugsnag/bugsnag-go v1.5.3
	github.com/bugsnag/panicwrap v1.2.0 // indirect
	github.com/certifi/gocertifi v0.0.0-20200211180108-c7c1fbc02894 // indirect
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gchaincl/dotsql v1.0.0
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/getsentry/sentry-go v0.5.1
	github.com/go-delve/delve v1.4.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/gorilla/csrf v1.7.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jawher/mow.cli v1.1.0 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/leekchan/accounting v0.0.0-20191218023648-17a4ce5f94d4
	github.com/lib/pq v1.3.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/newrelic/go-agent v3.4.0+incompatible // indirect
	github.com/newrelic/go-agent/v3 v3.4.0
	github.com/peterh/liner v1.2.0 // indirect
	github.com/robfig/cron v1.2.0
	github.com/robfig/cron/v3 v3.0.0
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.6 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/temoto/robotstxt v1.1.1 // indirect
	github.com/tideland/golib v4.24.2+incompatible // indirect
	github.com/tideland/gorest v2.15.5+incompatible // indirect
	go.starlark.net v0.0.0-20200306205701-8dd3e2ee1dd5 // indirect
	golang.org/x/arch v0.0.0-20200312215426-ff8b605520f4 // indirect
	golang.org/x/crypto v0.0.0-20200317142112-1b76d66859c6
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200317113312-5766fd39f98d // indirect
	golang.org/x/text v0.3.2
	google.golang.org/api v0.17.0
	google.golang.org/genproto v0.0.0-20200212174721-66ed5ce911ce
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
